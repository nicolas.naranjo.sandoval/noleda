public class ArrayRestaurante {
    public String[][] kawkaMenu = {
            {"waffle", "10.50"},
            {"waffle", "10.00"},
            {"waffle", "10.00"},
            {"waffle", "10.50"},
            {"waffle", "10.50"},
            {"waffle", "10.50"},
            {"waffle", "10.50"},
    };
    public String[][] sambolonMenu = {
            {"bolon", "5.00"},
            {"bolon", "5.00"},
            {"bolon", "5.00"},
            {"bolon", "5.00"},
            {"bolon", "5.00"},
            {"bolon", "5.00"},
            {"bolon", "5.00"},
    };
    public String[][] persaMenu = {
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
            {"yogurt", "9.00"},
    };
    public String[][] redcrabMenu = {
            {"cangrejo", "20.50"},
            {"cangrejo", "20.00"},
            {"cangrejo", "20.00"},
            {"cangrejo", "20.50"},
            {"cangrejo", "20.50"},
            {"cangrejo", "20.50"},
            {"cangrejo", "20.50"},
    };
    public String[][] ilbuccoMenu = {
            {"pasta", "18.50"},
            {"pasta", "18.00"},
            {"pasta", "18.00"},
            {"pasta", "18.50"},
            {"pasta", "18.50"},
            {"pasta", "18.50"},
            {"pasta", "18.50"},
    };
    public String[][] caramelMenu = {
            {"cafe", "5.50"},
            {"cafe", "5.00"},
            {"cafe", "5.00"},
            {"cafe", "5.50"},
            {"cafe", "5.50"},
            {"cafe", "5.50"},
            {"cafe", "5.50"},
    };
}
